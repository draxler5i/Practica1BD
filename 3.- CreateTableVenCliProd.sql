use SistemaVentas;

Create table Clientes
(
	Id int primary key NOT NULL,
	Nombre varchar(50) NULL,
	Apellido varchar(50) NULL,
	Edad int NULL
)

Create table Productos
(
	CodProducto int primary key NOT NULL,
	Nombre varchar(50) NOT NULL,
	Precio money NOT NULL
)

Create table Ventas
(
	Id int NOT NULL,
	IdProducto int NOT NULL,
	IdCliente int NOT NULL,
	Fecha date NULL,

	CONSTRAINT FK_Ventas_Productos
	FOREIGN KEY (IdProducto) REFERENCES Productos(CodProducto),

	CONSTRAINT FK_Ventas_Clientes
	FOREIGN KEY (IdCliente) REFERENCES Clientes(Id)
)
USE SistemaVentas ;

INSERT INTO Clientes
    VALUES (1, 'Carlos', 'Garrido', 23),
			(2, 'Roberto', 'Perez', 22),
			(3, 'Maria', 'Galindo', 21),
			(4, 'Carla', 'Mendoza', 31),
			(5, 'Kyle', 'Simpson', 34)

INSERT INTO Productos
    VALUES (1, 'Carlos', 23),
			(2, 'Roberto', 22),
			(3, 'Maria', 21),
			(4, 'Carla', 31),
			(5, 'Kyle', 34)

INSERT INTO Ventas
	VALUES (1, 3, 1, GETDATE()),
			(2, 4, 2, GETDATE()),
			(3, 5, 3, GETDATE()),
			(4, 2, 4, GETDATE()),
			(5, 1, 5, GETDATE())